document.addEventListener('DOMContentLoaded', (event) => {
    checkCoordinates();
    var myModal = new bootstrap.Modal(document.getElementById('player-name-modal'), { backdrop: "static", focus: true });
    myModal.show();
})

let board = [];
var currentPlayer = "x";
var playerXName = "Player X";
var playerOName = "Player O";
const xIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" fill="currentColor" class="bi bi-x-lg text-dark" viewBox="0 0 16 16"><path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/></svg>`;
const oIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" fill="currentColor" class="bi bi-circle text-danger" viewBox="0 0 16 16"><path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/></svg>`;

const checkCoordinates = () => {
    currentPlayer = "x";
    const player1El = document.querySelector("span#player1");
    player1El.classList.remove("bg-light", "text-dark");
    player1El.classList.add("bg-primary");
    const player2El = document.querySelector("span#player2");
    player2El.classList.remove("bg-primary");
    player2El.classList.add("bg-light", "text-dark");
    const xSize = document.getElementById("grid-size-x").value;
    const ySize = document.getElementById("grid-size-y").value;
    if (xSize && ySize) {
        createLayout(xSize, ySize);
    } else if (xSize) {
        createLayout(xSize, 9);
    } else if (ySize) {
        createLayout(9, ySize);
    } else {
        createLayout(9, 9);
    }
}

const resetAll = () => {
    currentPlayer = "x";
    const player1El = document.querySelector("span#player1");
    player1El.classList.remove("bg-light", "text-dark");
    player1El.classList.add("bg-primary");
    const player2El = document.querySelector("span#player2");
    player2El.classList.remove("bg-primary");
    player2El.classList.add("bg-light", "text-dark");
    document.getElementById("grid-size-x").value = "";
    document.getElementById("grid-size-y").value = "";
    createLayout(9, 9);
}

const createLayout = (x, y) => {
    const table = document.getElementById("game");
    table.innerHTML = "";
    for (let i = 0; i < y; i++) {
        const tr = document.createElement("tr");
        for (let j = 0; j < x; j++) {
            const td = document.createElement("td");
            // td.innerHTML = `${i}x${j}`;
            td.setAttribute("class", "cell");
            td.setAttribute("coordinates", `${i},${j}`);
            td.setAttribute("onclick", `cellClick(${i}, ${j})`);
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    for (let xIndex = 0; xIndex < x; xIndex++) {
        board[xIndex] = [];
        for (let yIndex = 0; yIndex < y; yIndex++) {
            board[xIndex][yIndex] = "";
        }
    }
}

const cellClick = (x, y) => {
    const cell = document.querySelector(`[coordinates="${x},${y}"]:not([selected])`);
    if (cell) {
        cell.setAttribute("selected", "true");
        cell.innerHTML = currentPlayer === "x" ? xIcon : oIcon;
        board[x][y] = currentPlayer;
        const winner = checkWinner(x, y, currentPlayer);
        if(winner) {
            var myModal = new bootstrap.Modal(document.getElementById('winner-modal'), { backdrop: "static", focus: true });
            const bodyModal = document.querySelector("#winner-modal .modal-body");
            bodyModal.innerHTML = `<span style="color: red">Winner: ${winner.includes("x") ? playerXName : playerOName}</span>`;
            myModal.show();
        } else {
            currentPlayer === "x" ? currentPlayer = "o" : currentPlayer = "x";
            switchPlayer();
        }
    }
}

const switchPlayer = () => {
    const players = document.querySelectorAll("span[player]");
    players.forEach(player => {
        player.classList.toggle("bg-primary");
        player.classList.toggle("bg-light");
        player.classList.toggle("text-dark");
    });
}

const checkWinner = (x, y, checkingPlayer) => {

    let count = 0;
    for (let index = x - 4; index <= x + 4; index++) {
        const currentPlayer = getPointValue(index, y);
        if(currentPlayer === checkingPlayer) {
            count++;
            if(count === 5) {
                return checkingPlayer;
            }
        } else {
            count = 0;
        }
    }

    count = 0;
    for (let index = y - 4; index <= y + 4; index++) {
        const currentPlayer = getPointValue(x, index);
        if(currentPlayer === checkingPlayer) {
            count++;
            if(count === 5) {
                return checkingPlayer;
            }
        } else {
            count = 0;
        }
    }

    let checkPointXBL = x + 4;
    let checkPointYBL = y - 4;
    const stopPointXTR = x - 4;
    const stopPointYTR = y + 4;
    
    let checkPointXTL = x - 4;
    let checkPointYTL = y - 4;
    const stopPointXBR = x + 4;
    const stopPointYBR = y + 4;
    
    count = 0;
    while(checkPointYBL <= stopPointYTR && checkPointXBL >= stopPointXTR) {
        const currentPlayer = getPointValue(checkPointXBL, checkPointYBL);
        if(currentPlayer === checkingPlayer) {
            count++;
            if(count === 5) {
                return checkingPlayer;
            }
        } else {
            count = 0;
        }
        checkPointYBL++;
        checkPointXBL--;
    }
    count = 0;
    while(checkPointYTL <= stopPointYBR && checkPointXTL <= stopPointXBR) {
        const currentPlayer = getPointValue(checkPointXTL, checkPointYTL);
        if(currentPlayer === checkingPlayer) {
            count++;
            if(count === 5) {
                return checkingPlayer;
            }
        } else {
            count = 0;
        }
        checkPointXTL++;
        checkPointYTL++;
    }
}

const getPointValue = (x, y) => {
    try {
        return board[x][y];
    } catch (error) {
        return "";
    }
}

const setPlayerName = (event) => {
    const playerName = event.target.value;
    const player = event.target.id;
    const doneButton = document.getElementById("player-set-button");
    if(!playerName) {
        event.target.classList.add("is-invalid");
        doneButton.setAttribute("disabled", "true");
    } else {
        if(player.includes("x-name")) {
            playerXName = playerName;
            document.getElementById("playerX").innerHTML = `Player: ${playerName}`;
        } else {
            playerOName = playerName;
            document.getElementById("playerO").innerHTML = `Player: ${playerName}`;
        }
        event.target.classList.remove("is-invalid");
    }
    let isValid = true;
    const players = document.querySelectorAll("#player-x-name, #player-o-name");
    players.forEach(item => {
        if(!item.value) {
            isValid = false;
        }
    });
    if(isValid) {
        doneButton.removeAttribute("disabled");
    }
}

